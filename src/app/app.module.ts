import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {FormsModule} from '@angular/forms';
import {DataService} from './services/data.service';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { TopnavComponent } from './topnav/topnav.component';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';
import { CartComponent } from './cart/cart.component';
import { ContactComponent } from './contact/contact.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
// import { BannerComponent } from './banner/banner.component';


@NgModule({
  declarations: [
    AppComponent,
    TopnavComponent,
    FooterComponent,
    AboutComponent,
    CartComponent,
    ContactComponent,
    RegisterComponent,
    LoginComponent,
    // BannerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot([{path:"About",component:AboutComponent},{path:"Cart",component:CartComponent},{path:"Contact",component:ContactComponent},
  {path:"Register",component:RegisterComponent},{path:"Login",component:LoginComponent}])
  
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
