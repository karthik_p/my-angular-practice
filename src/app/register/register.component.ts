import { Component, OnInit } from '@angular/core';
import { Register } from './register';
import {DataService} from '../services/data.service';
import {HttpModule} from '@angular/http';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register: Register;
  countries: Array<any>
  constructor(private dataservice:DataService) { 
  this.register = new Register();
  this.countries = this.dataservice.getCountryList();
    }

  ngOnInit() {
  }
  registerHandler() {
    console.log(this.register);
  }

}
