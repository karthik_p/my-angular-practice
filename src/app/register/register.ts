export class Register{
    firstName:String;
    lastName:String;
    userName:String;
    age:Number;
    gender:String;
    email:String;
    phoneNumber:Number;
    password:String;
    iAccept:Boolean;
}