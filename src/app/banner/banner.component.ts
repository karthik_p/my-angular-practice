import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  name:string;
  age:number;
  email:string;
  address:Address;
  hobbies:any[];
  constructor() { }

  ngOnInit() {
      this.name='Karthik';
      this.age=24;
      this.email='karthik.nani2011@gmail.com';
      this.address={
        street:'Sr nagar',
        city:'Hyderabad',
        state:'Telangana',
      }
      this.hobbies=['Watching movies', 'playing games','bike raiding'];
  }
  onClick(){
    //alert("Hello! I am an alert box!!");
    this.name="kalyan";
    this.hobbies.push('New Hobby');
  }  

  addHobby(hobby){
this.hobbies.unshift(hobby);
return false;
  }
  //this is for class method
  // titleClass='redline';
  // titleClass=true;
  titleClass={
    'redline':true,
    'bluebackground':true,
  }
  titleStyles={
    'color':'pink',
    'font-size':'2em',
  }
  isavailable = true;   //variable is set to true
}

interface Address{
  street:string;
  city:string;
  state:string;
}
