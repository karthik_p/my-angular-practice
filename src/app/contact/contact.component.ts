import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
allowNewServer=false;
serverCreatedStatus='no server created';
  constructor() { 
setTimeout(() => {
  this.allowNewServer=true;
}, 5000);

  }

  ngOnInit() {
  }
  onServerCreate(){
    this.serverCreatedStatus='server created succussfully';
  }

}
