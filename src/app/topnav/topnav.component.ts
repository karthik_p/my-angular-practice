import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {
  title:string;
  navLinks:Array<any>
  rightnavLinks:Array<any>
  constructor() { }

  ngOnInit() {
    this.title='Amibaba';
    this.navLinks=[
      {icon: 'fa fa-home',displayName:"About"},
      {icon: 'fa fa-product-hunt',displayName:"Cart"},
      {icon: 'fa fa-cart-plus',displayName:"Contact"}
    ];

    this.rightnavLinks=[
      {icon:'fa fa-product-hunt',displayName:"Register"},
      {icon:'fa fa-user-plus',displayName:"Login"}
    ];

  }

}
