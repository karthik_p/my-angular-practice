import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
@Injectable()
export class DataService {
  private countryNames: Array<any>

  constructor(private httpSvc: Http) {
   }
   getCountryList():Array<any>{
   this.countryNames=[{ text: "India", value: "IN" },
   { text: "Australia", value: "AU" },
   { text: "United States", value: "US" }];
   return this.countryNames;
   }
}
