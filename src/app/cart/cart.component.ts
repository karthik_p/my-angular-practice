import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  headtable:Array<any>
  row1:Array<any>
  row2:Array<any>
  row3:Array<any>
  constructor() { }

  ngOnInit() {
    this.headtable=[
      {displaytName:"Header 1"},
      {displaytName:"Header 2"},
      {displaytName:"Header 3"}
    ];
    this.row1=[
      {displaytName:"row 1"},
      {displaytName:"row 1"},
      {displaytName:"row 1"}
    ];
    this.row2=[
      {displaytName:"row 2"},
      {displaytName:"row 2"},
      {displaytName:"row 2"}
    ];
    this.row3=[
      {displaytName:"row 3"},
      {displaytName:"row 3"},
      {displaytName:"row 3"}
    ];
  }

}
